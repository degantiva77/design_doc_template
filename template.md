# Titulo: Prototipo De Una Aplicación Web Para El Almacenamiento De Información Sobre La Gestión De Residuos Electrónicos En La Localidad De Chapinero
---
## Overview: Problema a resolver
Descripción..

### Alcance(Scope)
Descripción..

#### Casos de uso
* Caso de uso 1 - Registrar Usuario
Descripción: Para el registro de un nuevo usuario el software solicitará al mismo la siguiente información: Nombre, Correo, Fecha de nacimiento, Número de teléfono y ocupación.
Una vez realizado el registro se enviará un correo de confirmación el cual le permitirá re dirigirse al menú principal de la aplicación. 

```plantuml

@startuml


Persona --> (Diligenciar Datos)
(Diligenciar Datos) -> (Validar Información) :include

@enduml
```

* Caso de uso 2 - Consultar reciclaje por usuario
Descripción: El usuario podrá visualizar la cantidad de puntos que posee, van a estar listados en una tabla con la fecha, lugar y cantidad de desechos electrónicos depositados, con una breve descripción del electrodoméstico. El administrador podrá ver esta descripción,  podrá eliminar o modificar los datos de la misma.
```plantuml
@startuml

Usuario --> (Consultar reciclaje almacenado ) 
Administrador --> (Consultar reciclaje almacenado) 
    
(Consultar reciclaje almacenado) -> (Listar Usuario) :include
(Consultar reciclaje almacenado) -> (Seleccionar Usuario) :include



@enduml
```
* Caso de uso 3 - Visualizar meta de reciclaje
Descripción: A cada usuario se le asignará una meta para poder reclamar su bonificación, por ejemplo: si el usuario quiere reclamar boletos para cine necesitará x cantidad de reciclaje. Esto se hace con el fin de incentivar al usuario a depositar sus electrodomésticos. Además, podrá ver su meta en semanas o en meses.

```plantuml
@startuml

Usuario --> (Visualizar Meta) 
    
(Visualizar Meta) -> (Meta Semanal) :extend
(Visualizar Meta) -> (Meta Mesual) :extend


@enduml
```
* Caso de uso 4 - Enviar mensaje al administrador.
Descipcion: El usuario podrá enviar y visualizar los mensajes, se presentarán varias opciones para modificar el texto adjuntado. Se tendrá un apartado para enviar archivos adjuntos y aparecerá una lista con los archivos adjuntos y el tipo de archivo.

```plantuml
@startuml
Usuario --> (Procesar publicacion de emnsaje)
(Procesar publicacion de emnsaje) -> (listar solicitudes pendientes) :extend
Administrado --> (listar solicitudes pendientes)
@enduml
```
* Caso de uso 5 - Consultar sitio de reciclaje.
Descripcion: El usuario podrá consultar su sitio de reciclaje más cercano, al ingresar le pedirá al usuario su ubicación y le permitirá ver el eco-punto más cercano.
```plantuml
@startuml
Usuario --> (sonsultar Sitio de Reciclaje)
@enduml
```
* Caso de uso 6 - Agregar Sponsor.
Descripcion: Un sponsor que no tiene que estar registrado en la aplicación, se podrá comunicar con un administrador para ofrecer sus servicios a los usuarios (Bonificaciones).
```plantuml
@startuml
Usuario --> (Listar Solicitudes Pendientes)
(Listar Solicitudes Pendientes)->(Agregar Sponsor) :include
(Redactar Peticion) -> (Listar Solicitudes Pendientes):include
Sponsor --> (Redactar Peticion)
@enduml
```


#### Out of Scope (casos de uso No Soportados)
Descripción...
* Caso de uso 1
* Caso de uso 2
* ...

---
## Arquitectura

### Diagramas
poner diagramas de secuencia, uml, etc
#Caso de uso Uno








### uml: class diagram
```plantuml
@startuml
package "customer domain" #DDDDDD {
    class Contact {
        + email
        + phone
    }

    class Address {
        + address1
        + address2
        + city
        + region
        + country
        + postalCode
        + organization
    }

    note right of Address 
        There are two types of 
        addresses: billing and shipping
    end note

    class Customer {
    }

    Customer *-- Contact
    Customer *-- ShippingAddress
    Customer *-- BillingAddress
    Customer *--{ SalesOrder

    class ShippingAddress <<Address>>
    class BillingAddress <<Address>>
    class SalesOrder {
        + itemDescription
        + itemPrice
        + shippingCost
        + trackingNumber
        + shipDate
    }
}
@enduml
```

### Modelo de datos
Poner diseño de entidades, Jsons, tablas, diagramas entidad relación, etc..

---
## Limitaciones
Lista de limitaciones conocidas. Puede ser en formato de lista.
Ej.
* Llamadas del API tienen latencia X
* No se soporta mas de X llamadas por segundo
---
## Costo
Descripción/Análisis de costos
Ejemplo:
"Considerando N usuarios diarios, M llamadas a X servicio/baseDatos/etc"
* 1000 llamadas diarias a serverless functions. $XX.XX
* 1000 read/write units diarias a X Database on-demand. $XX.XX
Total: $xx.xx (al mes/dia/año)
